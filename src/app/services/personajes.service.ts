import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Personajes } from '../models/Personajes';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})

export class PersonajesService {
  personajes:string = 'http://hp-api.herokuapp.com/api/characters/house/';
  estudiantes:string = 'http://hp-api.herokuapp.com/api/characters/students';
  profesores:string = 'http://hp-api.herokuapp.com/api/characters/staff ';

  constructor(private http:HttpClient) { }

  // Get PersonajesXCasa
  getPersonajes(value:string):Observable<Personajes[]> {
    return this.http.get<Personajes[]>(`${this.personajes}${value}`);
  }

  // Get Estudiantes
  getEstudiantes():Observable<Personajes[]> {
    return this.http.get<Personajes[]>(this.estudiantes);
  }

  // Get Profesores
  getProfesores():Observable<Personajes[]> {
    return this.http.get<Personajes[]>(this.profesores);
  }
}
