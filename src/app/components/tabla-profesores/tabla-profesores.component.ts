import { Component, Input, OnInit } from '@angular/core';
import { PersonajesService } from '../../services/personajes.service';

import { Personajes } from '../../models/Personajes';

@Component({
  selector: 'app-tabla-profesores',
  templateUrl: './tabla-profesores.component.html',
  styleUrls: ['./tabla-profesores.component.css']
})
export class TablaProfesoresComponent implements OnInit {

  @Input()
  personajes: Personajes[] = [];

  displayedColumns: string[] = ['name', 'patronus', 'age', 'image'];

  constructor(private personajesService:PersonajesService) { }

  ngOnInit() {
    this.personajesService.getProfesores().subscribe(p => {
      this.personajes = p;
    });
  }

}
