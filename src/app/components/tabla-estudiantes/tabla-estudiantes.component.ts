import { Component, Input, OnInit } from '@angular/core';
import { PersonajesService } from '../../services/personajes.service';

import { Personajes } from '../../models/Personajes';

@Component({
  selector: 'app-tabla-estudiantes',
  templateUrl: './tabla-estudiantes.component.html',
  styleUrls: ['./tabla-estudiantes.component.css']
})
export class TablaEstudiantesComponent implements OnInit {

  @Input()
  personajes: Personajes[] = [];

  displayedColumns: string[] = ['name', 'patronus', 'age', 'image'];

  constructor(private personajesService:PersonajesService) { }

  ngOnInit() {
    this.personajesService.getEstudiantes().subscribe(p => {
      this.personajes = p;
    });
  }
}
