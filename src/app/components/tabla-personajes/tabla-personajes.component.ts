import { Component, Input, OnInit } from '@angular/core';
import { PersonajesService } from '../../services/personajes.service';

import { Personajes } from '../../models/Personajes';

@Component({
  selector: 'app-tabla-personajes',
  templateUrl: './tabla-personajes.component.html',
  styleUrls: ['./tabla-personajes.component.css']
})
export class TablaPersonajesComponent implements OnInit {

  selected:string = 'slytherin';

  @Input()
  personajes: Personajes[] = [];

  displayedColumns: string[] = ['name', 'patronus', 'age', 'image'];

  constructor(private personajesService:PersonajesService) { }

  ngOnInit() {
    this.personajesService.getPersonajes(this.selected).subscribe(p => {
      this.personajes = p;
    });
  }

  getPersonajes(value:string) {
    this.personajesService.getPersonajes(value).subscribe(p => {
      this.personajes = p;
    });
  }
}
