import { WHITE_ON_BLACK_CSS_CLASS } from '@angular/cdk/a11y/high-contrast-mode/high-contrast-mode-detector';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Personajes } from 'src/app/models/Personajes';
import { PersonajesService } from 'src/app/services/personajes.service';

@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.component.html',
  styleUrls: ['./solicitud.component.css']
})
export class SolicitudComponent implements OnInit {

  @Input()
  personajes: Personajes[] = [];

  solicitud:Personajes[] = [];

  myForm = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.pattern('[a-zA-Z ]+')
    ]),
    age: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]*$')
    ]),
    patronus: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z ]+')
    ]),
  });

  displayedColumns: string[] = ['name', 'age', 'patronus'];

  constructor(private personajesService:PersonajesService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.solicitud.push({image:"",name: this.myForm.value.name, yearOfBirth: this.myForm.value.age, patronus: this.myForm.value.patronus});
    this.personajes = this.solicitud;
    this.myForm.reset();
  }
}
