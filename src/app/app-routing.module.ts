import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TablaPersonajesComponent } from './components/tabla-personajes/tabla-personajes.component';
import { AboutComponent } from './components/about/about.component';
import { TablaEstudiantesComponent } from './components/tabla-estudiantes/tabla-estudiantes.component';
import { TablaProfesoresComponent } from './components/tabla-profesores/tabla-profesores.component';
import { SolicitudComponent } from './components/solicitud/solicitud.component';

const routes: Routes = [
  { path: '', component: SolicitudComponent },
  { path: 'personajes', component: TablaPersonajesComponent},
  { path: 'estudiantes', component: TablaEstudiantesComponent },
  { path: 'profesores', component: TablaProfesoresComponent },
  { path: 'about', component: AboutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
