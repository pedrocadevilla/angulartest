import { Wand } from "./Wand";

export interface Personajes {
  image:string;
  name:string;
  patronus:string;
  yearOfBirth:number;
}
